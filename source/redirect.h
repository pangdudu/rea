/*
The "Redirect" class makes c++callbacks visible to ruby.

purpose:
make c++ callbacks available to ruby.

issues:
*	There is a reason why this file is not contained within 
	a namespace. This is because we wanna be easy with SWIG.
	I dont know if this is actual a problem, but im just assuming.
*/
#ifndef EMBEDRUBY_REDIRECT_H
#define EMBEDRUBY_REDIRECT_H

#include "library.h"

/*
purpose:
make c++ callbacks available to ruby

functions:
*	call_cpp()  invokes the virtual function gate->call_cpp().

constraints:
*	This object is managed from ruby, thus dont raise
	c++ exceptions. Instead raise ruby exceptions.

issues:
*	ruby-wrapper, the global_gate is ugly. But unfortunatly 
	I do not know any other ways to initialize this struct!
*/
class Redirect {
private:
	EMBEDRUBY::Gate *gate;
	static EMBEDRUBY::Gate *global_gate;
public:
	Redirect();
	~Redirect();
	void call_cpp();

	// bootstrapping 
	static void SetGate(EMBEDRUBY::Gate *parent);
	static void RubyInit();
};

#endif // EMBEDRUBY_REDIRECT_H
