/*
The only thing the public ever will see. No traces of ruby at all!
*/
#ifndef EMBEDRUBY_LIBRARY_H
#define EMBEDRUBY_LIBRARY_H

#include <stdexcept>
#include <iostream>

namespace EMBEDRUBY {

/*
purpose:
start/stop this library

constraints:
*	lib_begin() must be invoked before using this library.
*	lib_end() must be invoked after using this library.
*/
void lib_begin();
void lib_end();


/*
purpose:
base class for errors
*/
class Error : public std::exception {
public:
	virtual void explain(std::ostream &o) const throw() = 0;
};


/*
purpose:
example base class, which you can inhierit from and 
overwrite call_cpp().

issues:
*	It has a hidden implementation, located in library.cpp.
*/
class Impl; // pimpl
class Gate {
public:
	Gate();
	virtual ~Gate();

	void call_ruby();
	virtual void call_cpp();
private:
	Impl *pimpl;
};

} // end of namespace EMBEDRUBY

#endif // EMBEDRUBY_LIBRARY_H
