/*
Implementation of the "Gate" class + lib_begin()/lib_end()
*/
#include <iostream>
#include <stdexcept>
#include <ruby.h>

#include "library.h"
#include "rubycpp.h"
#include "redirect.h"

namespace EMBEDRUBY {
using std::cout;
using std::endl;
using std::ostream;

RUBY_CPP::Objects *objects = 0;

/*
purpose:
an instance of the ruby class "Gate"

functions:
*	invoke functions on the instance.
*/
class Impl {
private:
	Gate *parent;
	VALUE self;
public:
	Impl(Gate *parent) : parent(parent) {
		Redirect::SetGate(parent);
		self = RUBY_CPP::New("RubyGate");
		objects->Register(self);
	}
	~Impl() {
		objects->Unregister(self);
	}
	void call_ruby() {
		RUBY_CPP::Funcall(self, rb_intern("call_ruby"), 0); 
		/*
		This is intented to provoke a segfault.

		First we unregister our object and running GC.
		Next we try to access the "dead" object, this
		should result in a segfault.

		objects->Unregister(self);
		RUBY_CPP::Funcall(rb_cObject, rb_intern("sweep"), 0); 
		RUBY_CPP::Funcall(self, rb_intern("insert"), 0); 
		*/
	}
};

Gate::Gate() {
	cout << "gate.ctor: enter" << endl;
	pimpl = new Impl(this);
	cout << "gate.ctor: leave" << endl;
}

Gate::~Gate() {
	cout << "gate.dtor: enter" << endl;
	delete pimpl;
	cout << "gate.dtor: leave" << endl;
}

void Gate::call_ruby() {
	cout << "gate.call_ruby: enter" << endl;
	pimpl->call_ruby();
	cout << "gate.call_ruby: leave" << endl;
}

void Gate::call_cpp() {
	cout << "gate.call_cpp: hello" << endl;
}

/*
purpose:
ensure that ruby is only started/stopped once!

todo: 
*	use a singleton for this
*/
#ifdef EMBEDRUBY_SWIG
	const bool use_swig = EMBEDRUBY_SWIG;
#else
	const bool use_swig = false;
#endif

static bool lib_running = false;

void lib_begin() {
	if(lib_running)
		return;
	
	// initialize ruby itself
	ruby_init();
	ruby_init_loadpath();
	ruby_script("embed");

	// setup our own environment
	objects = new RUBY_CPP::Objects;
	Redirect::RubyInit();
	RUBY_CPP::Require("gate");

	lib_running = true;
	cout << "realib: started (" << 
		(use_swig ? "with" : "no") << "-swig)" << endl;
}

void lib_end() {
	if(lib_running == false)
		return;
	
	// teardown (reverse initialization order)
	delete objects;
	ruby_finalize();

	lib_running = false;
	cout << "realib: stopped" << endl;
}

} // end of namespace EMBEDRUBY
