/*
Tell SWIG that it should generate a "Redirect" class for us.
*/
%module Embed 

%{
#include "redirect.h"
%}

%name(Redirect) class Redirect {
public:
	Redirect();
	void call_cpp();
};
