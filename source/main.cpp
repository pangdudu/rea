#include <iostream>
#include <cstdlib>
#include <stdexcept>
#include <signal.h>

#include "library.h"

using std::cout;
using std::cerr;
using std::endl;
using std::exception;

class CppGate : public EMBEDRUBY::Gate {
public:
	void call_cpp() {
		cout << "cppgate.call_cpp: bingo" << endl;
	}
};

void Test() {
	CppGate c;
	c.call_ruby();
}

int SafeMain() {
	try {
		EMBEDRUBY::lib_begin();
		Test();
		EMBEDRUBY::lib_end();
		return 0;
	} catch(const EMBEDRUBY::Error &e) {
		cerr << "EXCEPTION (RUBY):" << endl;
		e.explain(cerr);
		return 1;
	} catch(const exception &e) {
		cerr << "EXCEPTION=" << e.what() << endl;
		return 1;
	}
}

void sig_handler(int x) {
	cerr << "something bad happened! x=" << x << endl;
	exit(-1);
}

int main() {
	cout << "main: enter" << endl;
	signal(SIGABRT, sig_handler);
	int status = SafeMain();
	cout << "main: leave (" << status << ")" << endl;
	return status;
}
