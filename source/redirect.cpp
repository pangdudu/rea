#include <iostream>
#include "redirect.h"
#include "rubycpp.h"

using std::cout;
using std::endl;

Redirect::Redirect() { 
	this->gate = Redirect::global_gate;
	cout << "redirect.ctor: hello" << endl;
}

Redirect::~Redirect() {
	cout << "redirect.dtor: hello" << endl;
}

void Redirect::call_cpp() {
	RUBY_TRY {
		cout << "redirect.call_cpp: enter" << endl;
		gate->call_cpp();
		cout << "redirect.call_cpp: leave" << endl;
	} RUBY_CATCH
}

void Redirect::SetGate(EMBEDRUBY::Gate *parent) {
	Redirect::global_gate = parent;
}

EMBEDRUBY::Gate *Redirect::global_gate = 0;


#if EMBEDRUBY_SWIG != 0
/* -----------------------------------
SWIG has generated the necessary 
wrapper code for us. All we have is
just to call Init_Embed();
----------------------------------- */

extern "C" void Init_Embed(); 

void Redirect::RubyInit() {
	Init_Embed();
}

#else
/* -----------------------------------
This is how it looks if you are 
writing the wrapper yourself.
As you can imagine this can quickly
become messy and confusing.
----------------------------------- */

#include <ruby.h>

namespace WRAP {

void Free(Redirect *p) {
	delete p;
}

VALUE Alloc(VALUE self) {
	Redirect* p = new Redirect();
	return Data_Wrap_Struct(self, 0, Free, p);
}

VALUE Call_cpp(VALUE self) {
	Redirect *p;
	Data_Get_Struct(self, Redirect, p);
	p->call_cpp();
	return self;
}

} // end of namespace WRAP

void Redirect::RubyInit() {
	VALUE module = rb_define_module("Embed");
	VALUE h = rb_define_class_under(module, "Redirect", rb_cObject);
	rb_define_alloc_func(h, WRAP::Alloc); 
	typedef VALUE (*HOOK)(...);
	rb_define_method(h, "call_cpp", reinterpret_cast<HOOK>(&WRAP::Call_cpp), 0);
}

#endif
